
local pcap = require"pcap"
local pcap_log_path = "/pcap" --"/data/suricata/lua/"
--cap = assert(pcap.open_offline(pcap.log))

cap = assert(pcap.open_dead())
dmp = assert(cap:dump_open("/pcap/cap0.pcap"))

function init (args)
    local needs = {}
	needs["packet"] = tostring(true)
    return needs
    --local needs = {}
    --needs["type"] = "packet"
    --return needs
end

function setup (args)
    name = "pcap.log"
    filename = SCLogPath() .. "/" .. name
    prefix_files = file_pcap_prefixes()
    local tid, tname, tgroup = SCThreadInfo()
    SCLogInfo("Tname: "..tostring(tid))
    tblstr = dump(args)
    print(tblstr)
    --list_file_name = pcap_file_names(tname, prefix_files)
    -- file = assert(io.open(filename, "a"))
    SCLogInfo("HTTP Log Filename " .. filename)
    http = 0
end

function log(args)
    ipver, srcip, dstip, proto, sp, dp, vlan0, vlan1 = SCPacketTuple()
    local sec, usec = SCPacketTimestamp()
    
    --print("Timestamp: "..sec.."\n")
    --print("Usec: "..usec.."\n")
    capdata = SCPacketPayload()
    timestamp = tonumber(sec.."."..usec)
    
    if(vlan0 > 0) then
        --print("Vlan 0: "..vlan0) 
        tblstr = dump(args)
        print(tblstr)
        if dmp then
            print("Save")
            assert(dmp:dump(capdata, timestamp, #capdata))
        end
    end
    
    
end


function deinit (args)
    SCLogInfo ("Close PCAP file and capture");
   -- file:close(file)
    cap:close()
    assert(dmp:dump("pcap closed"))
    dmp:close()
end

function isempty(s)
    return s == nil or s == ''
end

function table.find(f, l, key) -- find element f of l satisfying v(key)==f
    for k, v in pairs(l) do
        for k1,v1 in pairs(v) do
            if k1 == key and f==v[key] then
                return v
            end
        end
    end
    return nil
end

function file_pcap_prefixes()
    local vlan_files = {}
    for var=0,200 do
        local vlan_file = {}
        vlan_file['vlanid'] = var
        vlan_file['fileprefix'] = "pcap_v"..var.."_"
        table.insert(vlan_files, vlan_file)
    end
    return vlan_files
end

function dump(o)
    if type(o) == 'table' then
       local s = '{ '
       for k,v in pairs(o) do
          if type(k) ~= 'number' then k = '"'..k..'"' end
          s = s .. '['..k..'] = ' .. dump(v) .. ','
       end
       return s .. '} '
    else
       return tostring(o)
    end
end

--function pcap_file_names(tname, prefix_files)
--    local list_current_files={}
--    local prefixes = prefix_files
--    for k,v in pairs(prefixes) do
--        local r = init_pcap_name(v, tname)
--        table.insert(list_current_files, r)
--    end
--    return list_current_files
--end

--function init_pcap_name(row_prefix,tname)
--    local pcap_name = {}
--    local x = os.clock()
--    local r = row_prefix
--    if isempty(r)== false then
--        SCLogInfo(r)
--        for k,v in pair(r) do
--            pcap_name[k] =v
--        end
--    end
--    pcap_name['currentfile'] =pcap_log_path..row['fileprefix']..tname..'_'..x..'.pcap'
--    return pcap_name

--end

function fsize (file)
    local current = file:seek()      -- get current position
    local size = file:seek("end")    -- get file size
    file:seek("set", current)        -- restore position
    return size
end
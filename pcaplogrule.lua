--Author: <sroberts@wurldtech.com>
--Homepage: <https://github.com/sam-github/pcap-lua>
-- Lua library from git project to write pcap
local pcap = require"pcap"
--local ffi=require'ffi'
-- Writing Path
local pcap_log_path = "/pcap" --"/data/suricata/lua/"
local pcap_files ={}

--Size to recycle pcap file 500MB
local max_pcap_size = 524288000

function init (args)
    local needs = {}
	needs["packet"] = tostring(true)
    return needs
end

--Captures Packet and write pcap
function match(args)
    -- Gets packet details
    -- Todo: get it from args
    local ipver, srcip, dstip, proto, sp, dp, vlan0, vlan1 = SCPacketTuple()
    local sec, usec = SCPacketTimestamp()
    -- Get details from thread
    local tid, tname, tgroup = SCThreadInfo()
    -- Find Open pcap file
    local pcap_file,idx = table.find(vlan0..tid, pcap_files,'id')
    -- Create and Open PCAP file if not available
    if(pcap_file == nil) then
        pcap_file = init_pcap(vlan0,tid)
        table.insert(pcap_files, pcap_file)
        tblstr = dump(pcap_files)
        print(tblstr)
        -- file:close(file)
    end
    -- Check size of current PCAP and Recycle if necessary
    local size = fsize(pcap_file['file'])    
    if(size >= max_pcap_size*.98) then
        SCLogInfo ("Recycle file: "..size);
        pcap_file = recycle_pcap(pcap_file['id'], idx)
    end
    -- Prepare data to write to PCAP
    local capdata = args['packet']
    
    timestamp = tonumber(sec.."."..usec)
    dmp = pcap_file['dump']
    -- Save data to file
    if(vlan0 >0) then
        local vpos = string.find(capdata, "vlan")
        if(vpos == nil) then
            print('Vlan Pos: Nil')
        else
            print('Vlan Pos: '..vpos)
        end
        if dmp then
            print("Save")
            assert(dmp:dump(capdata, timestamp, wirelen))
        end
    end
end
-- Recycle PCAP once is full and create new PCAP file
function recycle_pcap(pcap_id, idx)
    local dump = pcap_files[idx]['dump']
    local filename = pcap_files[idx]['file']
    local cap = pcap_files[idx]['cap']
    pcap_files[idx]['cap'] = close_dump(cap,dump)
    rename_file(filename, pcap_id)
    pcap_files[idx]['file'] = pcap_log_path..'/.cap'..pcap_id..os.time()..'.pcap'
    pcap_files[idx]['cap'] = assert(pcap.open_dead())
    pcap_files[idx]['dump'] = assert(open_dump(pcap_files[idx]['cap'], pcap_files[idx]['file']))
    return pcap_files[idx]
end
-- Close PCAP file and free dump
function close_dump(cap, dump)
    cap:close()
    assert(dump:dump("pcap closed"))
    dump:close()
    return cap
end
-- Open or create new PCAP and ready to write
function open_dump(cap, filename)
    local dump = assert(cap:dump_open(filename))
    return dump
end
-- Rename Closed file and release it for next step
function rename_file(filename, pcap_id)
    SCLogInfo ("Rename PCAP file "..filename)    
    local newname = pcap_log_path..'/cap'..pcap_id..os.time()..'.pcap'
    ok, message = assert(os.rename (filename, newname))
    if(ok ~= true) then
        print(message)
    end
end
-- Create new PCAP file table with details
function init_pcap(vlan, tid)
    SCLogInfo ("Open PCAP file from vlan "..vlan..tid)    
    local pcap_file = {}
    pcap_file['id'] = vlan..tid
    pcap_file['tid'] = tid
    pcap_file['vlan'] = vlan
    pcap_file['file'] = pcap_log_path..'/.cap'..vlan..tid..os.time()..'.pcap'
    pcap_file['cap'] = assert(pcap.open_dead())
    pcap_file['dump'] = assert(open_dump(pcap_file['cap'],pcap_file['file']))    
    return pcap_file
end
--Close all files
function deinit (args)
    SCLogInfo ("Close PCAP file and capture");    
    for _,t in ipairs(pcap_files) do
        local dmp = t['dump']
        local filename = t['file']
        local cap = t['cap']
        local pcap_id = pcap_file['id']
        assert(dmp:dump("pcap closed"))        
        rename_file(filename, pcap_id)
        dmp:close()
    end
end

function isempty(s)
    return s == nil or s == ''
end
--Find element in multidimensional table given a certain key
function table.find(f, l, key) -- find element f of l satisfying v(key)==f
    for k, v in pairs(l) do
        for k1,v1 in pairs(v) do
            if k1 == key and f==v[key] then
                return v,k
            end
        end
    end
    return nil
end

function file_pcap_prefixes()
    local vlan_files = {}
    for var=0,200 do
        local vlan_file = {}
        vlan_file['vlanid'] = var
        vlan_file['fileprefix'] = "pcap_v"..var.."_"
        table.insert(vlan_files, vlan_file)
    end
    return vlan_files
end
--Debug function to dump a table structure
function dump(o)
    if type(o) == 'table' then
       local s = '{ '
       for k,v in pairs(o) do
          if type(k) ~= 'number' then k = '"'..k..'"' end
          s = s .. '['..k..'] = ' .. dump(v) .. ','
       end
       return s .. '} '
    else
       return tostring(o)
    end
end

--function pcap_file_names(tname, prefix_files)
--    local list_current_files={}
--    local prefixes = prefix_files
--    for k,v in pairs(prefixes) do
--        local r = init_pcap_name(v, tname)
--        table.insert(list_current_files, r)
--    end
--    return list_current_files
--end

--function init_pcap_name(row_prefix,tname)
--    local pcap_name = {}
--    local x = os.clock()
--    local r = row_prefix
--    if isempty(r)== false then
--        SCLogInfo(r)
--        for k,v in pair(r) do
--            pcap_name[k] =v
--        end
--    end
--    pcap_name['currentfile'] =pcap_log_path..row['fileprefix']..tname..'_'..x..'.pcap'
--    return pcap_name

--end
-- Check File Size and return size in Bytes
function fsize (filename)
    local file = assert(io.open(filename, "r"))
    local current = file:seek()      -- get current position
    local size = file:seek("end")    -- get file size
    file:seek("set", current)        -- restore position
    file:close()
    return size
end
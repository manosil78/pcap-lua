FROM centos/s2i-base-centos7
RUN yum install -y centos-release-scl && yum clean all -y
RUN cd /tmp
RUN yum -y install epel-release
RUN yum -y update
RUN yum -y install gcc libpcap-devel pcre-devel libyaml-devel file-devel \
	  zlib-devel jansson-devel nss-devel libcap-ng-devel libnet-devel tar make \
	  libnetfilter_queue-devel lua-devel readline-devel \
      git perl-libwww-perl perl-Crypt-SSLeay perl-Sys-Syslog \
      perl-Archive-Tar perl-LWP-Protocol-https python27 \
      python27-python-devel python27-python-setuptools \ 
	  python27-python-pip GeoIP.x86_64 GeoIP-devel net-tools --enablerepo=epel
RUN ldconfig
#Install Luajit
RUN wget http://luajit.org/download/LuaJIT-2.0.5.tar.gz
RUN tar -zxf LuaJIT-2.0.5.tar.gz
RUN cd LuaJIT-2.0.5 && make && make install
RUN ldconfig

COPY /suricata-4.0.4.tar.gz /tmp/
RUN tar -xvzf /tmp/suricata-4.0.4.tar.gz -C /tmp
RUN cd /tmp/suricata-4.0.4 && ./configure --enable-geoip--enable-luajit \ 
    --prefix=/usr --sysconfdir=/etc \
    --localstatedir=/var --enable-nfqueue --enable-lua --with-libnss-libraries=/usr/lib \
    --with-libnss-includes=/usr/include/nss \
    --with-libnspr-libraries=/usr/lib \
    --with-libluajit-includes=/usr/local/include/luajit-2.0/ \
    --with-libluajit-libraries=/usr/lib/ \
    --with-libnspr-includes=/usr/include/nspr && make && make install
RUN ldconfig
RUN mkdir /pcap
RUN chmod a+rwx -R /pcap
RUN mkdir /var/log/suricata
RUN mkdir /etc/suricata
RUN cp /tmp/suricata-4.0.4/classification.config /etc/suricata
RUN cp /tmp/suricata-4.0.4/reference.config /etc/suricata

# Install Pulled pork to maintain rule db
RUN cd /tmp
RUN mkdir /tmp/pulledpork
RUN mkdir /etc/pulledpork
RUN git clone https://github.com/shirkdog/pulledpork.git /tmp/pulledpork
RUN cd /tmp/pulledpork
RUN cp -rf /tmp/pulledpork/* /etc/pulledpork
RUN chmod +x /etc/pulledpork/pulledpork.pl
RUN cp -rf /tmp/pulledpork/etc/*.conf /etc/suricata/
RUN ls -la /etc/suricata
RUN mkdir -p /etc/suricata/rules/iplists
RUN touch /etc/suricata/rules/iplists/default.blacklist
COPY /pulledpork.conf /etc/suricata/
COPY /threshold.config /etc/suricata/
#Copy config files
COPY /suricata.yaml /etc/suricata/
RUN /etc/pulledpork/pulledpork.pl -c /etc/suricata/pulledpork.conf
#Install PCAP wrapper for Lua
COPY /pcap.so /usr/lib64/lua/5.1/
#Copy Lua script and rules
COPY /pcaplog.lua /lua-dependency/
COPY /pcaplogrule.lua /etc/suricata/rules/
COPY /pcaplog.rules /etc/suricata/rules/
#Folder for Unix socket and logs
RUN mkdir -p /var/run/suricata
RUN chmod a+rwx -R /var/run/suricata
RUN chmod a+rwx -R /var/log/suricata
#Delete log files before restarting
RUN rm -f /var/log/suricata/*
#Command line use to run suricata
CMD ["suricata","-c" ,"/etc/suricata/suricata.yaml", "-i","eth0", "-vvvv", "-k", "none"]
#CMD ["suricata","-c" ,"/etc/suricata/suricata.yaml", "--af-packet=eth0", "-vvvv", "-k", "none"]
#,"--unix-socket" ] 

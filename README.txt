
#pcap-lua

Lua script and Docker to run suricata for PCAP writing by VLAN ID. pcaprule.lua uses pcap-lua wrapper Homepage: https://github.com/sam-github/pcap-lua. I have also modified Suricata Lua to also be able to get VLAN id from Luajit. I have created a generic rule to capture all traffic Following command can be run for testing on Docker sudo docker run -it --net=host -v $(pwd)/logs:/var/log/suricata -v $(pwd)/data:/pcap suridocker
